'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		return queryInterface.addColumn('AnimalFounds', 'urgency', Sequelize.BOOLEAN);
	},

	down: async (queryInterface, Sequelize) => {
		return queryInterface.removeColumn('AnimalFounds', 'urgency');
	},
};
