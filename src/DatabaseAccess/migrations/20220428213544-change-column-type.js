'use strict';

module.exports = {
	up: async (queryInterface, Sequelize) => {
		Promise.all([
			queryInterface.changeColumn('AnimalFounds ', 'urgency', {
				type: Sequelize.BOOLEAN,
				allowNull: true,
			}),
		]);
	},

	down: async (queryInterface, Sequelize) => {
		Promise.all([
			queryInterface.changeColumn('AnimalFounds ', 'urgency', {
				type: Sequelize.BOOLEAN,
				allowNull: true,
			}),
		]);
	},
};
