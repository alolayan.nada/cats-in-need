import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { ApolloClient, InMemoryCache, gql, ApolloProvider, HttpLink } from '@apollo/client';

let token = localStorage.getItem('authToken');
const devBackendUrl = 'http://localhost:5080'; // Dev Mode
const prodBackendUrl = '/'; // Production mode

const client = new ApolloClient({
	cache: new InMemoryCache(),
	link: new HttpLink({
		uri: prodBackendUrl,
		headers: {
			authorization: token ? token : null,
		},
	}),
});

ReactDOM.render(
	<ApolloProvider client={client}>
		<App />
	</ApolloProvider>,
	document.getElementById('root')
);
