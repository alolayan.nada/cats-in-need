import React, { useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import Header from '../../components/Header';
import Sider from '../../components/Sider';
import GoogleMap from '../../components/GoogleMap';
import AnimalFoundForm from '../../components/AnimalFoundForm';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { gql, useMutation, useQuery } from '@apollo/client';
import { Paper } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import Box from '@material-ui/core/Box';
import AnimalList from '../../components/AnimalList';

function Copyright() {
	return (
		<Typography variant="body2" color="textSecondary" align="center">
			{'Copyright © '}
			<Link color="inherit" href="animals.help22@gmail.com">
				Animals in need
			</Link>{' '}
			{new Date().getFullYear()}
			{'.'}
		</Typography>
	);
}

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
	},
	appBarSpacer: theme.mixins.toolbar,
	content: {
		flexGrow: 1,
		height: '100vh',
		overflow: 'auto',
	},
	container: {
		paddingTop: theme.spacing(4),
		paddingBottom: theme.spacing(4),
	},
	paper: {
		padding: theme.spacing(2),
		display: 'flex',
		overflow: 'auto',
		flexDirection: 'column',
	},
	fixedHeight: {
		height: 100,
	},
}));

const CREATE_ANIMAL_FOUND = gql`
	mutation createAnimalFound(
		$description: String
		$locationName: String
		$lat: String
		$lng: String
		$userId: Int
		$urgency: Boolean
	) {
		createAnimalFound(
			description: $description
			locationName: $locationName
			lat: $lat
			lng: $lng
			userId: $userId
			urgency: $urgency
		)
	}
`;

const GET_ANIMALS_FOUND = gql`
	query getAnimalsFound {
		getAnimalsFound {
			id
			description
			locationName
			lat
			lng
			userId
			urgency
		}
	}
`;

export default function Dashboard(props) {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const [clearValues, setClearValues] = React.useState(false);
	const [userLat, setUserLat] = useState('');
	const [userLng, setUserLng] = useState('');
	const [AnimalFoundId, setAnimalFoundId] = useState('');
	const [AnimalLocation, setAnimalLocation] = useState('');
	const [AnimalDescription, setAnimalDescription] = useState('');
	const [AnimalAlt, setAnimalLat] = useState('');
	const [AnimalLat, setAnimalLng] = useState('');
	const [currentAnima, setCurrentAnima] = useState();
	const [user, setUser] = useState('');

	const [createAnimalFound, { error: graphQlError, data }] = useMutation(CREATE_ANIMAL_FOUND);
	const { data: animalsData } = useQuery(GET_ANIMALS_FOUND, {
		pollInterval: 500, // Update every 500ms);
	});

	useEffect(() => {
		if (localStorage.getItem('currentUser')) {
			setUser(JSON.parse(localStorage.getItem('currentUser')).name);
		}
	});

	const submitAnimalForm = async (values) => {
		try {
			if (localStorage.getItem('authToken')) {
				await createAnimalFound({
					variables: {
						userId: values.userId,
						description: values.description,
						locationName: values.address,
						lat: values.location.lat.toString(),
						lng: values.location.lng.toString(),
						urgency: values.urgency,
					},
				})
					.then((res) => {
						if (res.data.createAnimalFound) {
							setClearValues(true);
						}
					})
					.catch(console.log);
			}
		} catch (e) {
			console.error(e);
		}
	};

	const handleDrawerOpen = () => {
		setOpen(true);
	};
	const handleDrawerClose = () => {
		setOpen(false);
	};

	return (
		<div className={classes.root}>
			<CssBaseline />
			<Header open={open} handleDrawerOpen={handleDrawerOpen} title={user} logged={props.logged} />
			<Sider open={open} handleDrawerClose={handleDrawerClose} />
			<main className={classes.content}>
				<div className={classes.appBarSpacer} />
				<Container className={classes.container}>
					<Grid container>
						<Grid container item xs={12} sm={4}>
							<Paper elevation={3}>
								<AnimalFoundForm
									handleAnimalFormSubmit={submitAnimalForm}
									clearValues={clearValues}
								/>
								<Grid>
									{graphQlError && (
										<Alert severity="error">
											{graphQlError.graphQLErrors.map(({ message }, i) => (
												<span key={i}>{message}</span>
											))}
										</Alert>
									)}
									{data && <Alert severity="success">{data.createAnimalFound}</Alert>}
								</Grid>
							</Paper>
						</Grid>
						<Grid container item xs={12} sm={8}>
							{animalsData && animalsData.getAnimalsFound !== undefined && (
								<GoogleMap
									animalsFoundList={animalsData.getAnimalsFound}
									userLat={userLat}
									userLng={userLng}
								/>
							)}
						</Grid>
					</Grid>
					<Grid
						container
						item
						spacing={0}
						direction="column"
						alignItems="center"
						justify="center"
						style={{ minHeight: '15vh' }}
					></Grid>
					<Grid
						container
						item
						spacing={0}
						direction="column"
						alignItems="center"
						justify="center"
						style={{ minHeight: '80vh' }}
					>
						<Grid item xs={8}>
							{animalsData !== undefined && (
								<AnimalList animalsFoundList={animalsData.getAnimalsFound} />
							)}
						</Grid>
					</Grid>
					<Box mt={5}>
						<Copyright />
					</Box>
				</Container>
			</main>
		</div>
	);
}
