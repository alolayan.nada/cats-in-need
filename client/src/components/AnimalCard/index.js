import React from 'react';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
const AnimalCard = ({ animal, urgency }) => {
	return (
		<TableBody>
			<TableRow key={animal.id}>
				<TableCell component="th" scope="animal">
					{animal.id}
				</TableCell>
				<TableCell align="center">{animal.description}</TableCell>
				<TableCell align="center">{animal.locationName}</TableCell>
				<TableCell align="center">{animal.lat}</TableCell>
				<TableCell align="center">{animal.lng}</TableCell>
				<TableCell align="center">{animal.userId}</TableCell>
				<TableCell align="center">{String(animal.urgency)}</TableCell>
			</TableRow>
		</TableBody>
	);
};

export default AnimalCard;
