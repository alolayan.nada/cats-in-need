import React from 'react';

import Table from '@material-ui/core/Table';

import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useState } from 'react';
import AnimalCard from '../AnimalCard';
import tableCellClasses from '@material-ui/core/TableCell';
import { styled } from '@material-ui/core/styles';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
	[`&.${tableCellClasses.head}`]: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
	},
	[`&.${tableCellClasses.body}`]: {
		fontSize: 14,
	},
}));
const StyledTableRow = styled(TableRow)(({ theme }) => ({
	'&:nth-of-type(odd)': {
		backgroundColor: theme.palette.action.hover,
	},
	// hide last border
	'&:last-child td, &:last-child th': {
		border: 0,
	},
}));
export default function AnimalList({ animalsFoundList }) {
	const [urgent, seturgent] = useState('');

	return (
		<div>
			<TableContainer
				component={Paper}
				options={{
					rowStyle: {
						fontSize: 50,
					},
				}}
			>
				<Table sx={{ minWidth: 700 }} aria-label="customized table table">
					<TableHead>
						<StyledTableRow>
							<StyledTableCell align="center">Case# </StyledTableCell>
							<StyledTableCell align="center">
								Animal state description and contact information{' '}
							</StyledTableCell>
							<StyledTableCell align="center">Animal location name</StyledTableCell>
							<StyledTableCell align="center">Alt</StyledTableCell>
							<StyledTableCell align="center">Lat</StyledTableCell>
							<StyledTableCell align="center">posted by user id</StyledTableCell>
							<StyledTableCell align="center">urgency</StyledTableCell>
						</StyledTableRow>
					</TableHead>
					{animalsFoundList.map((animal) => {
						return <AnimalCard animal={animal} />;
					})}
				</Table>
			</TableContainer>
		</div>
	);
}
