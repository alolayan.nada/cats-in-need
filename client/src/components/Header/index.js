import React from 'react';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import logout from '../../utils/logout';
import { useState, useEffect } from 'react';

const useStyles = makeStyles((theme) => ({
	root: {
		display: 'flex',
	},
	toolbar: {
		paddingRight: 24, // keep right padding when drawer closed
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	appBarShift: {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	menuButton: {
		marginRight: 36,
	},
	menuButtonHidden: {
		display: 'none',
	},
	title: {
		flexGrow: 1,
	},
	button: {
		background: '#3f51b5',
		marginLeft: '10px',
	},
}));

const divStyle = {
	display: 'block',
	marginRight: 20,
};

const drawerWidth = 240;
const refreshTime = 1000;

function Header({ open, handleDrawerOpen, title, logged, islogged, inLoginPage }) {
	const [LoggedState, ToggleLogin] = useState(false);
	const classes = useStyles();

	useEffect(() => {
		const comInterval = setInterval(loginStatus, refreshTime); //This will refresh the data at regularIntervals of refreshTime
		return () => clearInterval(comInterval); //Clear interval on component unmount to avoid memory leak
	}, [islogged]);

	function loginStatus() {
		console.log(logged, 'logged');
		if (localStorage.getItem('currentUser')) {
			ToggleLogin(true);
		}
	}

	return (
		<div>
			<AppBar
				position="absolute"
				color="primary"
				className={clsx(classes.appBar, open && classes.appBarShift)}
			>
				<Toolbar className={classes.toolbar}>
					{/* <IconButton
						edge="start"
						color="inherit"
						aria-label="open drawer"
						onClick={handleDrawerOpen}
						className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
					>
						<MenuIcon />
					</IconButton> */}
					<Typography variant="h3">Animals in need </Typography>
					<div></div>

					<Typography
						component="h1"
						variant="h6"
						color="inherit"
						noWrap
						className={classes.title}
					></Typography>

					<Typography variant="h5">
						<span STYLE="text-decoration:underline" style={divStyle}>
							{title}
						</span>
					</Typography>
					{inLoginPage ? (
						<Button
							variant="contained"
							color="secondary"
							onClick={() => {
								window.location.replace('/Dashboard');
							}}
						>
							Home
						</Button>
					) : !LoggedState ? (
						<Button
							variant="contained"
							color="secondary"
							onClick={() => {
								logout();
							}}
						>
							Login
						</Button>
					) : (
						<Button
							variant="contained"
							color="secondary"
							onClick={() => {
								logout();
							}}
						>
							Logout
						</Button>
					)}
				</Toolbar>
			</AppBar>
		</div>
	);
}

export default Header;
